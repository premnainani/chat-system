
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import javax.swing.JPanel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Chetna & Prem
 */
public class StatusChecker {
    
//    static HashMap<String, String> deliveredCheckingList = new HashMap<>(); 
    // Changing value type from String to ArrayList now because if prem is sending msg to raj and raj is        offline and ravi also sends msg to raj so this deliveredCheckingList update prem as ravi and later when raj comes online.. only ravi's msgs are updated while prem's msg will still say that he's offline so  making a array so that every other user's status gets updated!
    
    static HashMap<String, HashSet<String>> deliveredCheckingList = new HashMap<>();
    static HashMap<String, HashSet<String>> readCheckingList = new HashMap<>();
    
//    private static HashMap<String, HashMap<String,JPanel>> chatPaneCollectionSet = new HashMap<>();
    private static HashMap<String, Chat> chatPaneCollectionSet = new HashMap<>();
    
//    public static void addChatPaneCollection(String name, HashMap chatPaneCollection){
//        chatPaneCollectionSet.put(name, chatPaneCollection);
//    }
    
    public static void addChatPaneCollection(String name, Chat chat){
        chatPaneCollectionSet.put(name, chat);
    }

    public static HashMap<String,Chat> getChatPaneCollectionSet(){
        return chatPaneCollectionSet;
    }
    
}
