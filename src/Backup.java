/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Chetna & Prem
 */

import java.io.File;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Backup implements StatusConstraints{
    
    static File myFile;
    static FileReader reader;
    static FileWriter writer;
    
    synchronized public static void addData(String callerUserName, String fromUsername, String toUsername, String message, String time){
        try{
            
            // Code to create a json object with these details
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("fromUser",fromUsername);
            jsonObject.put("toUser",toUsername);
            jsonObject.put("message",message);
            jsonObject.put("time", time);
            if(callerUserName.equals(fromUsername)){
                jsonObject.put("status",MESSAGE_SENT);
            }
            
            // Code to fetch (username).json file and add the created jsonObject to file
            myFile = new File(callerUserName.toLowerCase()+".json");
            reader = new FileReader(myFile);
            JSONObject mainObject = (JSONObject) new JSONParser().parse(reader);
            JSONArray data = (JSONArray) mainObject.get("messages");
            
            data.add(jsonObject);
            writer = new FileWriter(myFile);
            writer.write(mainObject.toJSONString());
            writer.flush();
            
        }catch(FileNotFoundException e){
            System.out.println(e.getMessage());
        }catch(IOException e){
            System.out.println(e.getMessage());
        }catch(ParseException e){
            System.out.println(e.getMessage());
        }
        finally{
            try{
                writer.close();
                reader.close();
            }catch(IOException e){
                System.out.println("Issue in finally block of addData : " + e);
            }
        }
    }
    
    
    synchronized public static void changeStatusInJSON(String callerUserName, String destName, int changeStatus){
        try {
            myFile = new File(callerUserName.toLowerCase()+".json");
            reader = new FileReader(myFile);
            
            JSONObject mainObject = (JSONObject) new JSONParser().parse(reader);
//            System.out.println(mainObject.toJSONString());
            JSONArray data = (JSONArray) mainObject.get("messages");
            
            for(int i=1; i <= data.size(); i++){
                JSONObject jsonObject = (JSONObject)data.get(data.size() - i);
                if(jsonObject.containsKey("status")){
                    if(jsonObject.get("toUser").equals(destName)){
                        long status = (long)jsonObject.get("status");
                        if(changeStatus == MESSAGE_DELIVERED){
                            if(status != MESSAGE_SENT){
    //                            System.out.println("Break Done!");
                                break;
                            }
                        }else{
                            if(status == MESSAGE_READ){
    //                            System.out.println("Break");
                                break;
                            }
                        }
                    jsonObject.put("status", changeStatus);
                    }
                }
            }
            writer = new FileWriter(myFile);
            writer.write(mainObject.toJSONString());
            writer.flush();

        } catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage());
        } catch (IOException ex) {
            System.out.println("Issue in changeStatusInJSON : " + ex);
        } catch (ParseException ex) {
            System.out.println(ex.getMessage());
        }
        finally{
            try{
                reader.close();
            }catch(IOException e){
                System.out.println("Issue in finally block of changeStatusInJSON : " + e);
            }
        }
    }
    
    
}
