
import java.util.HashMap;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Chetna & Prem
 */
public class ClientHelper {
    
    static HashMap<String, Client> clients = new HashMap<>();
    
    static Client client;
    
    public static HashMap<String, Client> getClients(){
        return clients;
    }
    
    public static Client getClient(String username){
        if(clients.containsKey(username)){
            return clients.get(username);
        }
        return null;
    }
    
    public static void addClient(String username){
        client = new Client(username);
        clients.put(username, client);
    }
    
}
