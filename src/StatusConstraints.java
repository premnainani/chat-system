/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Chetna & Prem
 */
public interface StatusConstraints {
    int MESSAGE_SENT = 0;
    int MESSAGE_DELIVERED = 1;
    int MESSAGE_READ = 2;
    
    String SINGLE_TICK = "MESSAGE IS SENT";
    String DOUBLE_TICK = "MESSAGE IS DELIVERED";
    String DOUBLE_TICK_BLUE = "MESSAGE IS READ";
}
