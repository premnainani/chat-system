
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Chetna & Prem
 */
public class MessageOutput extends Thread{
    PrintWriter output;
    private final Socket socket;
    
    MessageOutput(Socket socket){
        this.socket = socket;
    }
    
    public void run(){
        try{
            output = new PrintWriter(socket.getOutputStream(), false);
            while(true){
                output.flush();
            }
        }catch(IOException e){
            System.out.println(e.getMessage());
        }
    }
}
