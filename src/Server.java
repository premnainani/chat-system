
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import java.util.HashMap;
import java.util.Scanner;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Chetna & Prem
 */

public class Server{
    
    volatile static HashMap<String, Client> clients = new HashMap<>();
    static HashMap<String, PrintWriter> allActiveClients = new HashMap<>();
    ServerSocket server;
    static Client client;
    
    public static void main(String args[]) throws Exception{
        Server server = new Server();
        server.startServer();
    }
    
    public static Client getClient(String username){
        if(clients.containsKey(username)){
            return clients.get(username);
        }
        return null;
    }
    
    public static void addClient(String username){
        client = new Client(username);
        clients.put(username, client);
        System.out.println(clients.size());
        
    }
//    public HashMap<String,Client> getClients(){
//        return ClientHelper.clients;
//    }
    
    public void startServer(){
        System.out.println("Server Started Listening ...... ");
        try{
            server = new ServerSocket(8086);
    //        initializeJSON();
            while(true){
                Socket clientSocket = server.accept();
                System.out.println("In server activeClients Size is " + allActiveClients.size());
                Scanner scanner = new Scanner(clientSocket.getInputStream());
//                PrintWriter p = new PrintWriter(clientSocket.getOutputStream(), true);
//                p.println("Holla!");
                System.out.println("Waiting for Clients's Name");
                String name = scanner.nextLine();
                System.out.println("Client Sent Name : " + name);
                ServerInput si = new ServerInput(clientSocket, allActiveClients, this, name);
                si.start();
//                new ServerOutput(clientSocket).start();
            }
        }catch(IOException e){
            System.out.println(e.getMessage());
        }
    }
            
    public static void initializeJSON(){
        File jsonFile = new File("data.json");
        if(!jsonFile.exists()){
            try(FileWriter writer = new FileWriter("data.json")){
                JSONObject mainObject = new JSONObject();
                JSONArray allChats = new JSONArray();
                mainObject.put("messages", allChats);
                JSONObject.writeJSONString(mainObject, writer);
                writer.flush();
                writer.close();
            }catch(IOException e){
                System.out.println(e);
            }
        }
    }
}
