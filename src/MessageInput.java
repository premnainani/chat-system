
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.HashMap;
import javax.swing.JOptionPane;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Chetna & Prem
 */
public class MessageInput extends Thread implements StatusConstraints{
    
//    ChatPanel pane = new ChatPanel();
    private final Socket socket;
    String name;
    BufferedReader input;
    Chat myChat;
    
    public MessageInput(String name, Socket socket){
        this.name = name;
        this.socket = socket;
    }
    
    @Override
    public void run(){
        try{
            String receiverName, message, receiveTime;
            String result[];
            input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            System.out.println("Hello there, I am MessageInput thread");
            myChat = new Chat(name, socket);
//            StatusChecker.addChatPaneCollection(name, myChat.chatPaneCollection);
            StatusChecker.addChatPaneCollection(name, myChat);
            
            System.out.println("In MessageInput size is " + StatusChecker.getChatPaneCollectionSet().size());
            myChat.setVisible(true);
//            socket.setSoTimeout(5000);

            while(true){
                message = input.readLine();
                if(message.equals("EXIT123456789EXIT987654321")){
                    break;
                }
                Login.changeStatus(name);
                result = message.split(",", 3);
                receiverName = result[0];
                receiveTime = result[1];
                message = result[2];
//                System.out.println("Server sent : " + receiverName + " sent : " + message + " at Time : " +                 receiveTime);
                myChat.addMessageToChat(false, receiverName, message, receiveTime);
//                Backup.addData(name, receiverName, name, message, receiveTime);
//                JOptionPane.showMessageDialog(null , message);
            }
            System.out.println("Client Thread of " + name + " is died");
        }catch(IOException ie){
            System.out.println(ie.getMessage());
        }
    }
}
