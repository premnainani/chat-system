
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Chetna & Prem
 */
public class MysqlConnect {
    static Connection connection;
    
    public static Connection connectDB(){
        try{
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/chatdata", "premnainani", "prem@123");
            return connection;
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, "Connection Failed" + e);
            return null;
        }
    }
}
