
import java.net.Socket;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.UnknownHostException;
import java.util.HashSet;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Prem
 */
public class Client {
    // make private and getters
    private String name;
    private Socket socket;
    BufferedReader input;
    PrintWriter output;
    private HashSet<Client> contacts;
           
    Client(String name){
        try{
            this.name = name;
            socket = new Socket("localhost", 8086);
//            System.out.println(socket.hashCode());
            input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            output = new PrintWriter(socket.getOutputStream(), true);
            contacts = new HashSet<>();
            new MessageInput(name, socket).start();
//            new MessageOutput(socket).start();
        }catch(UnknownHostException e){    
            System.out.println("Issue in socket : " + e.getMessage());
        }catch(IOException e){
            System.out.println("Issue in IO : " + e.getMessage());
        }
    }
    
    // Getters
    Socket getSocket(){
        return this.socket;
    }
    
    HashSet<Client> getContacts(){
        return this.contacts;
    }
    
    String getName(){
        return this.name;
    }
    
}
    